package ru.itis.webflux.webfluxservice.services;


import reactor.core.publisher.Flux;
import ru.itis.webflux.webfluxservice.entities.Item;


public interface Service {
    Flux<Item> getAll();
}
