package ru.itis.webflux.webfluxservice.clients;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import ru.itis.webflux.webfluxservice.entities.Item;

import java.util.Arrays;

@Component
public class DumbWebClient implements Client {

    private WebClient client;

    public DumbWebClient(@Value("${dumbService.api.url}") String url) {
        client = WebClient.builder()
                .baseUrl(url)
                .build();
    }

    @Override
    public Flux<Item> getAll() {
        return client.get()
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .flatMap(clientResponse -> clientResponse.bodyToMono(Item[].class))
                .flatMapIterable(Arrays::asList);
    }
}
