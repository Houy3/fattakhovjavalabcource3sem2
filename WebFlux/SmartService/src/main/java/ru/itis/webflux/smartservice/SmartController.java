package ru.itis.webflux.smartservice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@RequestMapping("/all")
public class SmartController {

    @GetMapping()
    public List<Item> smart() {
        return List.of( new Item("Smart1"), new Item("Smart2"));
    }
}
