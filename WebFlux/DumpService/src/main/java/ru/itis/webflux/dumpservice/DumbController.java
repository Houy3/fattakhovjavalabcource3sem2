package ru.itis.webflux.dumpservice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@RequestMapping("/all")
public class DumbController {

    @GetMapping()
    public List<Item> dumb() throws InterruptedException {
        Thread.sleep(7000);
        return List.of( new Item("Dumb1"), new Item("Dumb2"));
    }
}
