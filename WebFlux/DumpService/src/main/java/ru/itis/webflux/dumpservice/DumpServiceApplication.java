package ru.itis.webflux.dumpservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DumpServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DumpServiceApplication.class, args);
    }

}
