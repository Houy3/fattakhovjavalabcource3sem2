package core

import "go.mongodb.org/mongo-driver/bson/primitive"

type CV struct {
	Id     primitive.ObjectID `bson:"_id,omitempty"`
	User   string             `bson:"user,omitempty"`
	Age    int32              `bson:"age,omitempty"`
	Skills []string           `bson:"skills,omitempty"`
}
