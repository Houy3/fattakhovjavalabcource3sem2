package core

import "go.mongodb.org/mongo-driver/bson/primitive"

type Job struct {
	Id           primitive.ObjectID `bson:"_id,omitempty"`
	Organization string             `bson:"organization,omitempty"`
	MinUserAge   int32              `bson:"minUserAge,omitempty"`
	MaxUserAge   int32              `bson:"maxUserAge,omitempty"`
	Requirements []string           `bson:"requirements,omitempty"`
}
