package ru.itis.hrservice.dto;

import lombok.Builder;
import java.util.List;


@Builder
public class CVDto {

    public String id;

    public String user;

    public int age;

    public List<String> skills;
}
