package ru.itis.hrservice.services.grpcClient;

import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;
import ru.itis.hrservice.dto.CVDto;
import ru.itis.hrservice.grpc.pb.Cv;
import ru.itis.hrservice.grpc.pb.CvServiceGrpc;
import ru.itis.hrservice.grpc.pb.WhoAreYouParams;
import ru.itis.hrservice.services.CVService;

import java.util.List;
import java.util.stream.StreamSupport;

@Service
public class CVServiceGrpcClient implements CVService {

    @GrpcClient("cv-service")
    private CvServiceGrpc.CvServiceBlockingStub client;


    @Override
    public List<CVDto> listAll() {
        Iterable<Cv> iterable  = () -> client.listAll(WhoAreYouParams.getDefaultInstance());
        return StreamSupport.stream(iterable.spliterator(), false)
                .map(this::map)
                .toList();
    }

    protected CVDto map(Cv CV) {
        return CVDto.builder()
                .id(CV.getId())
                .user(CV.getUser())
                .age(CV.getAge())
                .skills(CV.getSkillsList().stream().toList())
                .build();
    }
}
