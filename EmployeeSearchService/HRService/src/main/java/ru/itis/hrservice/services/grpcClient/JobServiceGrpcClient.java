package ru.itis.hrservice.services.grpcClient;

import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;
import ru.itis.hrservice.dto.JobDto;
import ru.itis.hrservice.grpc.pb.Job;
import ru.itis.hrservice.grpc.pb.JobServiceGrpc;
import ru.itis.hrservice.grpc.pb.WhoAreYouParams;
import ru.itis.hrservice.services.JobService;

import java.util.List;
import java.util.stream.StreamSupport;

@Service
public class JobServiceGrpcClient implements JobService {


    @GrpcClient("job-service")
    private JobServiceGrpc.JobServiceBlockingStub client;


    @Override
    public List<JobDto> listAll() {
        Iterable<Job> iterable  = () -> client.listAll(WhoAreYouParams.getDefaultInstance());
        return StreamSupport.stream(iterable.spliterator(), false)
                .map(this::map)
                .toList();
    }


    protected JobDto map(Job job) {
        return JobDto.builder()
                .id(job.getId())
                .organization(job.getOrganization())
                .minUserAge(job.getMinUserAge())
                .maxUserAge(job.getMaxUserAge())
                .requirements(job.getRequirementsList().stream().toList())
                .build();
    }
}
