package ru.itis.hrservice.dto;

import lombok.Builder;

import java.util.List;

@Builder
public class JobDto {

    public String id;

    public String organization;

    public int minUserAge;

    public int maxUserAge;

    public List<String> requirements;

}
