package ru.itis.hrservice.services;

import ru.itis.hrservice.dto.CandidateDto;

import java.util.List;

public interface HRService {

    List<CandidateDto> listAllCandidates();
}
