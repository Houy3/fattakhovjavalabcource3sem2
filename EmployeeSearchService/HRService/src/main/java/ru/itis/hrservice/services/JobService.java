package ru.itis.hrservice.services;


import ru.itis.hrservice.dto.JobDto;

import java.util.List;

public interface JobService {
    List<JobDto> listAll();
}
