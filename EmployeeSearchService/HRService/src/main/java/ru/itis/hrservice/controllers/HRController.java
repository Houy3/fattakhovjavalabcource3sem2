package ru.itis.hrservice.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.hrservice.dto.CVDto;
import ru.itis.hrservice.dto.CandidateDto;
import ru.itis.hrservice.dto.JobDto;
import ru.itis.hrservice.services.CVService;
import ru.itis.hrservice.services.HRService;
import ru.itis.hrservice.services.JobService;

import java.util.List;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class HRController {

    private final JobService jobService;
    private final CVService cvService;
    private final HRService hrService;

    @GetMapping("/jobs")
    public List<JobDto> allJob() {
        return jobService.listAll();
    }

    @GetMapping("/cvs")
    public List<CVDto> allCV() {
        return cvService.listAll();
    }

    @GetMapping("/candidates")
    public List<CandidateDto> allCandidates() {
        return hrService.listAllCandidates();
    }
}
