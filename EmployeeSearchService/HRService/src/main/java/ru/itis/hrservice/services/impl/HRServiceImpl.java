package ru.itis.hrservice.services.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.hrservice.dto.CVDto;
import ru.itis.hrservice.dto.CandidateDto;
import ru.itis.hrservice.dto.JobDto;
import ru.itis.hrservice.services.CVService;
import ru.itis.hrservice.services.HRService;
import ru.itis.hrservice.services.JobService;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class HRServiceImpl implements HRService {

    private final JobService jobService;
    private final CVService cvService;


    @Override
    public List<CandidateDto> listAllCandidates() {
        var jobs = jobService.listAll();
        var cvs = cvService.listAll();
        var candidates = new ArrayList<CandidateDto>();

        for (var cv : cvs)
            for (var job : jobs)
                if (isCVSuitable(cv, job))
                    candidates.add(new CandidateDto(cv, job));

        return candidates;
    }

    protected boolean isCVSuitable(CVDto cv, JobDto job) {
        return job.minUserAge <= cv.age && cv.age <= job.maxUserAge
                && isUserHaveAllNeededSkills(cv.skills, job.requirements);
    }

    protected boolean isUserHaveAllNeededSkills(List<String> skills, List<String> requirements) {
        for (var requirement : requirements)
            if (!skills.contains(requirement))
                return false;

        return true;
    }
}
