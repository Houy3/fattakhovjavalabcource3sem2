package ru.itis.hrservice.dto;

import java.util.List;

public class CandidateDto {

    public String cvId;

    public String user;

    public int age;

    public List<String> skills;

    public String jobId;

    public String organization;

    public int minUserAge;

    public int maxUserAge;

    public List<String> requirements;

    public CandidateDto(CVDto cv, JobDto job) {
        cvId = cv.id;
        user = cv.user;
        age = cv.age;
        skills = cv.skills;
        jobId = job.id;
        organization = job.organization;
        minUserAge = job.minUserAge;
        maxUserAge = job.maxUserAge;
        requirements = job.requirements;
    }
}
