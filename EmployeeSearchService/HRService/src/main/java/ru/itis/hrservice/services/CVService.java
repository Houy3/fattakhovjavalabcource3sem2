package ru.itis.hrservice.services;

import ru.itis.hrservice.dto.CVDto;

import java.util.List;

public interface CVService {
    List<CVDto> listAll();
}
